#!/bin/bash
#
# This file is part of GetTor, a Tor Browser distribution system.
#
# :authors: hiro <hiro@torproject.org>
#           see also AUTHORS file
#
# :copyright:   (c) 2008-2019, The Tor Project, Inc.
#
# :license: This is Free Software. See LICENSE for license information.
#
# Updates all the files in all the endpoints.
#

cd ~/releases
git checkout master
git branch -D torbrowser-releases

git fetch --all --prune

git add .
git commit -am 'Create release branch'
git checkout -b torbrowser-releases
git push -f --follow-tags origin torbrowser-releases

rclone delete gdrive:releases

for row in $(
    curl -s 'https://aus1.torproject.org/torbrowser/update_3/release/downloads.json' |
    jq -r '.downloads'
  ); do
    r=$(
      echo ${row} |
      egrep -o 'https?://[^ ]+' |
      tr -d '",'
    );
    if [[ $r = *[!\ ]* ]]; then
      # Update GitLab
      git fetch --all
      wget $r
      git add .
      git commit -m '[dist ci] commit from CI runner - update with new torbrowser downloads'
      diffs=$(git diff origin/torbrowser-releases)
      if [ -z "$diffs" ]; then
          echo "No new releases"
      else
          git push -f --follow-tags origin torbrowser-releases
      fi

      f=${r##*/}

      # Update Google Drive
      rclone copy $f gdrive:releases

      # Update Internet Archive
      ia upload ${f} $f --remote-name=$f --metadata="title:${f}" --metadata="mediatype:software" --metadata="collection:open_source_software"

      rm $f

    fi;
done

